module.exports = [
    {
        type: 'TEST', // an identifier for you task
        queue: 'test', // the CONSUMER_QUEUE_ID or consumerQueueId to handle the queue
        handler: 'Test.dequeue', // the handler of the tasks of this type
        description: 'any remarks you want to add'
    }
];