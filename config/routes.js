module.exports = {

  preMiddlewares: [
    '* requestLog requestParseURLEncoded requestParseBody passString'
  ],

  routes: [

    'GET / PublicController.index',
    'GET /helloworld PublicController.helloworld',
    'GET /users PublicController.getUsers',
    'GET /publish PublicController.publish',
    'POST /products PublicController.addProduct'

  ],

  postMiddlewares: []


}
