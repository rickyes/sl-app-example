const { App: SuperApp } = require('@shopline/sl-express')

class App extends SuperApp {

  async connectDependencies() {

    try { await super.connectDependencies() }catch(e) { throw e }

  }

  async disconnectDependencies() {

    try { await super.disconnectDependencies() }catch(e) { throw e }

  }

  async startService() {

    if (this.config.app.role == "CONSUMER") {

      await this.startConsumer()

    }

    await this.startExpress()

  }


}

let app = new App();
module.exports = app;
