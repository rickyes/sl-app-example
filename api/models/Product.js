class Product extends MongooseModel {

  static schema() {

    return {

      name: { type: String, required: true },
      price: { type: Number, required: true }

    }

  }

}

module.exports = Product
