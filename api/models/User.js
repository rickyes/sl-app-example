class User {
  
  static transform(users) {
    
    if (!Array.isArray(users)) return []
    
    return users.map(u => {
      u.company = app.helpers.companyHelper()
      return u
    })

  }

}

module.exports = User