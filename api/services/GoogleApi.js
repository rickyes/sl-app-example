const axios = require('axios')

const GET_USERS_API = 'https://jsonplaceholder.typicode.com/todos'

class GoogleApi {

  static async getUsers() {

    const response = await axios.get(GET_USERS_API)
    return app.models.User.transform(response.data)

  }
}

module.exports = GoogleApi