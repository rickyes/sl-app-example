class PublicContoller {

  constructor(app) {
    this.app = app
  }

  async index(req, res) {
    
    return res.send('Hello world')
  
  }

  async helloworld(req, res) {

    return res.send(res.locals.user)

  }

  async getUsers(req, res) {

    const { services } = this.app

    try {

      const users = await services.GoogleApi.getUsers()
      
      return res.send({
        code: 200,
        data: users,
      })

    } catch (error) {

      console.error(error)
      
      return res.send({
        code: error.code,
        message: '系统开小差去了'
      })

    }

  }

  async publish(req, res) {

    const { models } = this.app

    await models.Test.enqueue({
      firstName: 'ricky',
      sex: '男',
    })

    return res.send({
      code: 200
    })

  }

  async addProduct(req, res) {
    const { Product } = this.app.models
    const product = new Product({
      name: req.body.name,
      price: req.body.price,
    })
    product.save(err => {
      const data = {
        code: 200,
        message: '添加成功',
      };
      if (err) {
        data.code = err.code,
        data.message = '添加商品失败'
      }
      return res.send(data)
    })
  }
  
}

module.exports = PublicContoller
  