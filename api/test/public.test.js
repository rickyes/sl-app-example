const { assert } = require('chai')
const mm = require('mm')
const app = require('../../server')
const request = require('supertest')(app)

describe('http request', () => {

  it('response helloworld string', (done) => {
    request
      .get('/helloworld')
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.text, 'helloworld');
        done();
      })
  })

  it('response users jsonArray with the comany attribute', (done) => {
    request
      .get('/users')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.isArray(res.body.data);
        assert.property(res.body.data[0], 'company');
        done();
      })
  })

  it('response add product success', (done) => {
    request
      .post('/products')
      .send({
        name: 'ricky',
        price: 100,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.code, 200);
        done();
      })
  })

})

